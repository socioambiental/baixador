import os
import mistune

def readme(readme_file):
    if os.path.isfile(readme_file):
        readme               = open(readme_file)
        markdown             = mistune.Markdown()
        return markdown(readme.read())

    return None
