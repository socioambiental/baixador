#!/usr/bin/env python3
#
# Baixador web app.
#

#
# Requirements
#
from flask import Flask, escape, request, render_template, url_for, jsonify
import analytics
import inventory

#
# Our app
#
name    = "Salva & Guarda"
app     = Flask(__name__)
version = "0.0.1"

#
# HTML pages
#

# Index
@app.route('/')
def main_redirect():
    from flask import redirect
    return redirect('/v1')

# Main route
@app.route('/v1')
def index():
    title = request.args.get("title", "Index")

    # Cases
    #cases = analytics.Manager()

    # Currently using the prioritized list
    #import pandas
    #sites = pandas.read_csv('batches/priorities.csv')
    #sites = sites.fillna('-')

    # Diskspace
    import os
    disk = '/media/dados'
    if os.path.exists(disk):
        # Thanks https://stackoverflow.com/questions/1094841/reusable-library-to-get-human-readable-version-of-file-size#1094933
        def sizeof_fmt(num, suffix='B'):
            for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
                if abs(num) < 1024.0:
                    return "%3.1f%s%s" % (num, unit, suffix)
                num /= 1024.0
            return "%.1f%s%s" % (num, 'Yi', suffix)

        from shutil import disk_usage
        #usage = { k : sizeof_fmt(v) for k, v in disk_usage(disk)._asdict() }
        #usage = { k : sizeof_fmt(v) for k, v in disk_usage(disk) }
        usage = sizeof_fmt(disk_usage(disk).used)
    else:
        usage = 0

    #return render_template('index.html', name=name, title=title, cases=cases.index(), sites=sites.to_html(classes="table table-striped tablesorter"))
    #return render_template('index.html', name=name, title=title, cases=cases.index(), sites=sites.iterrows())
    #return render_template('index.html', name=name, title=title, cases=cases.index())
    return render_template('index.html', name=name, title=title, usage=usage)

# API
@app.route('/v1/api')
def api():
    title = request.args.get("title", "API")

    return render_template('api.html', name=name, title=title)

# About
@app.route('/v1/about')
def about():
    import os
    from readme import readme

    title  = request.args.get("title", "About")
    readme = readme(os.path.dirname(__file__) + '/../../' + 'README.md')

    return render_template('about.html', name=name, title=title, readme=readme)

# References
@app.route('/v1/references')
def references():
    title = request.args.get("title", "References")
    cases = analytics.Manager()

    return render_template('references.html', name=name, title=title, cases=cases.index())

# Clipping
@app.route('/v1/clipping')
def clipping():
    title = request.args.get("title", "Clipping")
    cases = analytics.Manager()

    return render_template('clipping.html', name=name, title=title)

# Analysis
@app.route('/v1/analysis/<string:day>/<string:site>')
def analysis(day, site):
    title = request.args.get("title", "Analysis")
    case  = analytics.Case(day, site)

    return render_template('analisys.html', name=name, title=title, case=case.run())

#
# API
#

# Index
@app.route("/v1/api/spec")
def spec():
    """
    API specification
    ---
    tags:
      - api
    responses:
      201:
        description: OpenAPI Specification
    """
    from flask_swagger import swagger

    swag                    = swagger(app)
    swag['info']['version'] = version
    swag['info']['title']   = name + " API"

    return jsonify(swag)

# Priorities
@app.route('/v1/api/inventory/')
def priorities():
    """
    Return the URL inventory
    ---
    tags:
      - inventory
    definitions:
      - schema:
          id: URL
          properties:
            name:
             type: string
             description: The endpoint URL
    responses:
      201:
        description: Inventory data
    """
    sites = inventory.Manager()

    return jsonify(sites.index())

# Cases
@app.route('/v1/api/analysis/')
def cases():
    """
    Return available analysis
    ---
    tags:
      - analysis
    responses:
      201:
        description: List of available analysis
    """
    cases = analytics.Manager()

    return jsonify(cases.index())

# Clipping
@app.route('/v1/api/clipping/')
def news():
    """
    Return available news
    ---
    tags:
      - news
    responses:
      201:
        description: List of news items
    """
    import pandas
    import os

    dirname  = os.path.dirname(os.path.abspath(__file__))
    clipping = pandas.read_csv(dirname + os.sep + '..' + os.sep + '..' + os.sep + 'dumps/clipping.csv')

    return jsonify(clipping.to_dict('records'))

#
# Main procedure
#
if __name__ == "__main__":
    #app.run(host="0.0.0.0", port="5000", debug=True, use_debugger=True)
    #app.run(host="0.0.0.0", port="5000")
    app.run()
