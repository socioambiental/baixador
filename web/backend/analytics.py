import glob
import os
from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relation, sessionmaker

class Manager:
    def index(self):
        path   = os.path.dirname(__file__) + '/../../analysis'
        cases  = [ { folder: os.listdir(path + os.sep + folder) } for folder in os.listdir(path) ]
        #cases = glob.glob(path + '/**/*.db', recursive=True)

        return cases

class Case:
    def __init__(self, day, site):
        self.data         = { }
        self.data['day']  = day
        self.data['site'] = site
        self.data['path'] = os.path.dirname(__file__) + '/../../analysis/' + day + '/' + site

    def database(self):
        database_file = self.data['path'] + os.sep + 'compare.db'
        if os.path.isfile(database_file):
            Base         = declarative_base()
            self.engine = create_engine('sqlite:///' + database_file)

            table_model_old = { '__tablename__': 'old',
                                'id'                  : Column(Integer, primary_key=True),
                                'content_type'        : Column(String,  nullable=False, unique=False), # Content-Type
                                'content_length'      : Column(String,  nullable=False, unique=False), # Content-Length
                                'warc_type'           : Column(String,  nullable=False, unique=False), # WARC-Type
                                'warc_date'           : Column(String,  nullable=False, unique=False), # WARC-Date
                                'warc_record_id'      : Column(String,  nullable=False, unique=False), # WARC-Record-ID
                                'warc_target_uri'     : Column(String,  nullable=False, unique=False), # WARC-Target-URI
                                'warc_ip_address'     : Column(String,  nullable=False, unique=False), # WARC-IP-Address
                                'warc_block_digest'   : Column(String,  nullable=False, unique=False), # WARC-Block-Digest
                                'warc_payload_digest' : Column(String,  nullable=False, unique=False), # WARC-Payload-Digest
                                'warc_warcinfo_id'    : Column(String,  nullable=False, unique=False), # WARC-Warcinfo-ID
                                'statusline'          : Column(String,  nullable=False, unique=False),
                                'status'              : Column(Integer, nullable=False, unique=False),
                    }
            table_old       = type('Old', (Base,), table_model_old)

            table_model_new = { '__tablename__': 'new',
                                'id'                  : Column(Integer, primary_key=True),
                                'content_type'        : Column(String,  nullable=False, unique=False), # Content-Type
                                'content_length'      : Column(String,  nullable=False, unique=False), # Content-Length
                                'warc_type'           : Column(String,  nullable=False, unique=False), # WARC-Type
                                'warc_date'           : Column(String,  nullable=False, unique=False), # WARC-Date
                                'warc_record_id'      : Column(String,  nullable=False, unique=False), # WARC-Record-ID
                                'warc_target_uri'     : Column(String,  nullable=False, unique=False), # WARC-Target-URI
                                'warc_ip_address'     : Column(String,  nullable=False, unique=False), # WARC-IP-Address
                                'warc_block_digest'   : Column(String,  nullable=False, unique=False), # WARC-Block-Digest
                                'warc_payload_digest' : Column(String,  nullable=False, unique=False), # WARC-Payload-Digest
                                'warc_warcinfo_id'    : Column(String,  nullable=False, unique=False), # WARC-Warcinfo-ID
                                'statusline'          : Column(String,  nullable=False, unique=False),
                                'status'              : Column(Integer, nullable=False, unique=False),
                    }
            table_new       = type('New', (Base,), table_model_new)

            Base.metadata.create_all(self.engine)

            Session = sessionmaker(bind=self.engine)
            session = Session()

            self.data['not_found_in_old'] = session.query(table_old).filter(table_old.warc_target_uri.ilike('%' + self.data['site'] + '%')).filter(table_old.status.like('%404%')).order_by('warc_target_uri').all()
            self.data['not_found_in_new'] = session.query(table_new).filter(table_new.warc_target_uri.ilike('%' + self.data['site'] + '%')).filter(table_new.status.like('%404%')).order_by('warc_target_uri').all()

            missing                           = text("SELECT DISTINCT warc_target_uri, status FROM old "
                                                     "WHERE warc_target_uri LIKE :site AND warc_target_uri NOT IN (SELECT DISTINCT warc_target_uri FROM new)")
            #self.data['missing']             = session.execute(missing, { 'site': "%"            + self.data['site'] + "%" }).fetchall()
            self.data['missing_http_no_www']  = session.execute(missing, { 'site': "http://"      + self.data['site'] + "%" }).fetchall()
            self.data['missing_http_www']     = session.execute(missing, { 'site': "http://www."  + self.data['site'] + "%" }).fetchall()
            self.data['missing_https_no_www'] = session.execute(missing, { 'site': "https://"     + self.data['site'] + "%" }).fetchall()
            self.data['missing_https_www']    = session.execute(missing, { 'site': "https://www." + self.data['site'] + "%" }).fetchall()

            new_not_found                           = text("SELECT DISTINCT warc_target_uri, status FROM new "
                                                           "WHERE status LIKE '%404%' AND warc_target_uri LIKE :site AND warc_target_uri NOT IN (SELECT DISTINCT warc_target_uri FROM old)")
            #self.data['new_not_found']             = session.execute(new_not_found, { 'site': "%"            + self.data['site'] + "%" }).fetchall()
            self.data['new_not_found_http_no_www']  = session.execute(new_not_found, { 'site': "http://"      + self.data['site'] + "%" }).fetchall()
            self.data['new_not_found_http_www']     = session.execute(new_not_found, { 'site': "http://www."  + self.data['site'] + "%" }).fetchall()
            self.data['new_not_found_https_no_www'] = session.execute(new_not_found, { 'site': "https://"     + self.data['site'] + "%" }).fetchall()
            self.data['new_not_found_https_www']    = session.execute(new_not_found, { 'site': "https://www." + self.data['site'] + "%" }).fetchall()

    def run(self):
        from readme import readme
        self.data['readme'] = readme(self.data['path'] + os.sep + 'README.md')

        self.database()

        return self.data
