'use strict';

import { pages  } from './pages.js';
import { footer } from './footer.js';

// About
export var about = new Vue({
  i18n       : pages.i18n,
  el         : '#about',
  delimiters : ['${', '}'],
  data       : {
    locale: pages.defaultLocale,
  },
  mounted: function() {
    this.localeTransition();
  },
  methods: {
    localeTransition: function() {
      var self = this;

      pages.bus.$on('locale-transition', function(value) {
        self.locale = value;
      });
    },
  },
});
