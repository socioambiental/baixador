'use strict';

import { pages     } from './pages.js';
import { inventory } from './inventory.js';

// Index
export var index = new Vue({
  i18n       : pages.i18n,
  el         : '#index',
  delimiters : ['${', '}'],
  components : {
    'inventory': inventory,
  },
  data       : {
    locale   : pages.defaultLocale,
    analysis : [],
  },
  mounted: function() {
    this.localeTransition();

    // Requests do not need FQDN, but need trailing slash to avoid redirects
    axios.get('/v1/api/analysis/')
      .then(response => {
        for (var date in response.data[0]) {
          for (var site in response.data[0][date]) {
            this.analysis.push({
              date: date,
              site: response.data[0][date][site],
            });
          }
        }

        //this.analysis = response.data;
      });
  },
  methods: {
    localeTransition: function() {
      var self = this;

      pages.bus.$on('locale-transition', function(value) {
        self.locale = value;
      });
    },
  },
});
