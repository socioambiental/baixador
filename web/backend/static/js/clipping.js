'use strict';

import { pages  } from './pages.js';
import { footer } from './footer.js';

// clipping
export var clipping = new Vue({
  i18n       : pages.i18n,
  el         : '#clipping',
  delimiters : ['${', '}'],
  data       : {
    //url    : 'https://archive.org/download/@salvaguarda/@salvaguarda_web-archive.json',
    url      : '/v1/api/clipping/',
    clipping : {},
    count    : 0,
    locale   : pages.defaultLocale,
  },
  mounted: function() {
    this.localeTransition();
    this.getClipping();
  },
  methods: {
    localeTransition: function() {
      var self = this;

      pages.bus.$on('locale-transition', function(value) {
        self.locale = value;
      });
    },
    getClipping: function() {
      axios.get(this.url)
        .then(response => {
          this.clipping = response.data;
          this.count    = Object.keys(response.data).length;
        });
    },
  },
});
