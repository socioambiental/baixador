'use strict';

import { messages } from './messages.js';

// Default locale
const defaultLocale = 'pt-br';

// Variables for static pages
export var pages = {
  defaultLocale: defaultLocale,

  // Localization engine
  i18n: new VueI18n({
    locale  : defaultLocale,
    messages: messages,
  }),

  // Holds application state
  state: {},

  // A bus for intercommunication
  bus: new Vue(),
}
