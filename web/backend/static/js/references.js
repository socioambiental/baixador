'use strict';

import { pages  } from './pages.js';
import { footer } from './footer.js';

// references
export var references = new Vue({
  i18n       : pages.i18n,
  el         : '#references',
  delimiters : ['${', '}'],
  data       : {
    locale: pages.defaultLocale,
  },
  mounted: function() {
    this.localeTransition();
  },
  methods: {
    localeTransition: function() {
      var self = this;

      pages.bus.$on('locale-transition', function(value) {
        self.locale = value;
      });
    },
  },
});
