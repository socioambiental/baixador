'use strict';

import { pages  } from './pages.js';
import { footer } from './footer.js';

// About
export var api = new Vue({
  i18n       : pages.i18n,
  el         : '#api',
  delimiters : ['${', '}'],
  data       : {
    locale: pages.defaultLocale,
  },
  mounted: function() {
    this.localeTransition();

    // Setup Swagger
    // See https://github.com/swagger-api/swagger-ui/blob/master/dist/index.html
    window.onload = function() {
      const swagger = SwaggerUIBundle({
        url         : '/v1/api/spec',
        dom_id      : '#swagger-ui',
        deepLinking : true,
        presets     : [
          SwaggerUIBundle.presets.apis,
          SwaggerUIStandalonePreset
        ],
        plugins     : [
          SwaggerUIBundle.plugins.DownloadUrl
        ],
        layout      : "StandaloneLayout"
      });
    }
  },
  methods: {
    localeTransition: function() {
      var self = this;

      pages.bus.$on('locale-transition', function(value) {
        self.locale = value;
      });
    },
  },
});
