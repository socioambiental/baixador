'use strict';

// Thanks http://stackoverflow.com/questions/2090551/parse-query-string-in-javascript#2091331
export var getQueryVariable = function(variable) {
  // First remove trailing slash
  var search = window.location.search.replace(/\/+$/, "");
  var query  = search.substring(1);
  var vars   = query.split('&');

  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split('=');

    if (decodeURIComponent(pair[0]) == variable) {
      return decodeURIComponent(pair[1]);
    }
  }

  return null;
};

// Thanks https://stackoverflow.com/questions/5999118/how-can-i-add-or-update-a-query-string-parameter#6021027
export var updateQueryString = function(key, value, url) {
  if (!url) {
    url = window.location.href;
  }

  var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi");
  var hash;

  if (re.test(url)) {
    if (typeof value !== 'undefined' && value !== null)
      return url.replace(re, '$1' + key + "=" + value + '$2$3');
    else {
      hash = url.split('#');
      url  = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');

      if (typeof hash[1] !== 'undefined' && hash[1] !== null) {
        url += '#' + hash[1];
      }

      return url;
    }
  }
  else {
    if (typeof value !== 'undefined' && value !== null) {
      var separator = url.indexOf('?') !== -1 ? '&' : '?';
      hash          = url.split('#');
      url           = hash[0] + separator + key + '=' + value;

      if (typeof hash[1] !== 'undefined' && hash[1] !== null) {
        url += '#' + hash[1];
      }

      return url;
    }
    else {
      return url;
    }
  }
};
