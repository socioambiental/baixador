'use strict';

import { pages             } from './pages.js';
import { getQueryVariable  } from './common/url_query.js';
import { updateQueryString } from './common/url_query.js';

// Index
export var footer = new Vue({
  i18n       : pages.i18n,
  el         : '#footer',
  delimiters : ['${', '}'],
  data       : {
    locale: pages.defaultLocale,
  },
  mounted: function() {
    this.setLang();
  },
  methods: {
    getQueryVariable: getQueryVariable,
    updateQueryString: updateQueryString,
    setLang: function() {
      var userLang = (navigator.language || navigator.userLanguage).replace(/(-|_).*$/, '');
      var lang     = this.getQueryVariable('lang');

      // If lang is not specified in the URL, try to guess the user language
      // Fallback to the default language
      if (lang == undefined || lang == 'undefined' || lang == 'null') {
        if (pages.i18n.messages[userLang] != undefined) {
          lang = userLang;
        }
        else {
          lang = pages.defaultLocale;
        }
      }

      this.localeTransition(lang);
      //this.setTitle();
    },
    localeTransition: function(value) {
      this.locale       = value;
      var title         = pages.i18n.messages[value].name;
      var url           = this.updateQueryString('lang', value);
      pages.i18n.locale = value;

      // See https://stackoverflow.com/questions/824349/modify-the-url-without-reloading-the-page#3354511
      //     https://stackoverflow.com/questions/2494213/changing-window-location-without-triggering-refresh
      //window.location.href = url;
      window.history.replaceState({}, title, url);

      // Wrap on $nextTick to give time to other apps to register on this event
      this.$nextTick(function() {
        pages.bus.$emit('locale-transition', value);
      });
    },
  },
  computed: {
    footerUrl: function() {
      return 'https://rodape.socioambiental.org/?site=salvaguarda&sponsors=dd&lang=' + this.locale;
    },
  },
});
