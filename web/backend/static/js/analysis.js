'use strict';

import { pages  } from './pages.js';
import { footer } from './footer.js';

// Analysis
export var analysis = new Vue({
  i18n       : pages.i18n,
  el         : '#analysis',
  delimiters : ['${', '}'],
  data       : {
    locale: pages.defaultLocale,
  },
  mounted: function() {
    this.localeTransition();
  },
  methods: {
    localeTransition: function() {
      var self = this;

      pages.bus.$on('locale-transition', function(value) {
        self.locale = value;
      });
    },
  },
});
