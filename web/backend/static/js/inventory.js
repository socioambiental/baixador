'use strict';

import { pages } from './pages.js';

// Inventory
export var inventory = Vue.component('inventory', {
  i18n       : pages.i18n,
  template   : '#inventory-template',
  delimiters : ['${', '}'],
  data       : function () {
    return {
      locale  : pages.defaultLocale,
      sites   : [],
      results : [],
      search  : '',
      filename: '',
      csv     : null,
      count   : 0,
    }
  },
  mounted: function() {
    this.localeTransition();

    // Requests do not need FQDN, but need trailing slash to avoid redirects
    //axios.get(document.location.origin + '/v1/api/priorities/')
    axios.get('/v1/api/inventory/')
      .then(response => {
        this.sites   = response.data;
        this.results = response.data;
        this.count   = Object.keys(response.data).length;

        this.$nextTick(function() {
          this.tableExport();

          jQuery('.table-inventory').tablesorter({
            theme: 'bootstrap',
          });

          // See http://tablesorter.com/docs/example-empty-table.html
          //     http://tablesorter.com/docs/example-ajax.html
          //jQuery(".table-indicators").trigger("update");
          //this.tableExport();
        });
      });
  },
  methods: {
    localeTransition: function() {
      var self = this;

      pages.bus.$on('locale-transition', function(value) {
        self.locale = value;
      });
    },
    // See https://vuejs.org/v2/guide/computed.html#Watchers about _debounce usage
    searchTable: _.debounce(
      function(query) {
        // Thanks to http://stackoverflow.com/a/38151393
        var regexp  = new RegExp(query.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&"), "i");
        var results = { };

        // Simple transition effect
        //this.indicators = { }

        if (query == '') {
          results = this.sites;
        }
        else {
          for (var item in this.sites) {
            if (regexp.test(this.sites[item].url)) {
              results[item] = this.sites[item];
            }
            else if (regexp.test(this.sites[item].institution)) {
              results[item] = this.sites[item];
            }
          }
        }

        this.results = results;
        this.count   = Object.keys(results).length;

        jQuery(".table").trigger("update");

        this.$nextTick(function() {
          this.tableExport(query);

          // See http://tablesorter.com/docs/example-empty-table.html
          //     http://tablesorter.com/docs/example-ajax.html
          jQuery(".table-inventory").trigger("update");
        });
      }, 500
    ),
    clear: function() {
      this.search = '';
    },
    tableExport: function(query) {
      var csv = jQuery('.table-inventory').first().table2csv('return', { "excludeColumns": ".csv-noexport", });

      if (query != null) {
        var suffix = '-' + query.replace(' ', '_');
      }
      else {
        var suffix = '';
      }

      this.filename = 'salvaguarda' + suffix + '.csv';
      this.csv      = 'data:text/csv;charset=UTF-8,' + encodeURIComponent(csv);
    },
    // Thanks http://stackoverflow.com/questions/10420352/ddg#14919494
    humanFileSize: function(bytes, si) {
      var thresh = si ? 1000 : 1024;

      if (Math.abs(bytes) < thresh) {
        return bytes + ' B';
      }

      var units = si
        ? ['kB','MB','GB','TB','PB','EB','ZB','YB']
        : ['KiB','MiB','GiB','TiB','PiB','EiB','ZiB','YiB'];

      var u = -1;

      do {
        bytes /= thresh;
        ++u;
      } while(Math.abs(bytes) >= thresh && u < units.length - 1);

      return bytes.toFixed(1) +' ' + units[u];
    },
    formatNumber: function(value) {
      if (!isNaN(value)) {
        value = Number(value);
        value = value.toLocaleString(this.locale);
      }

      return value;
    },
  },
  watch: {
    search: function(query) {
      this.searchTable(query);
    },
  },
});
