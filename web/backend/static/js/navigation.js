'use strict';

import { pages  } from './pages.js';
import { footer } from './footer.js';

// Index
export var navigation = new Vue({
  i18n       : pages.i18n,
  el         : '#navigation',
  delimiters : ['${', '}'],
  data       : {
    locale: pages.defaultLocale,
  },
  mounted: function() {
    this.localeTransition();
  },
  methods: {
    localeTransition: function() {
      var self = this;

      pages.bus.$on('locale-transition', function(value) {
        self.locale = value;
      });
    },
    i18nUrl: function(url) {
      return url + '?lang=' + this.locale;
    },
  },
  watch: {
    locale: function(value) {
      footer.localeTransition(value);
    }
  },
});
