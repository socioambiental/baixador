#!/usr/bin/env python3

import os
import re
import json

class Manager:
    def index(self):
        import os

        dirname = os.path.dirname(os.path.abspath(__file__))
        file    = dirname + os.sep + '..' + os.sep + '..' + os.sep + 'inventory/downloads/inventory.json'

        if os.path.exists(file):
            descriptor = open(file)
            data       = json.load(descriptor)

            descriptor.close()
            return data
