# Inventário: Ministério do Planejamento

Esta pasta contém inventários de domínios baseadas nos seguintes conjuntos de dados:

* [Domínios gov.br - Portal Brasileiro de Dados Abertos](http://dados.gov.br/dataset/dominios-gov-br).
* [Domínios gov.br Registrados - Gestão de Domínios](http://dominios.governoeletronico.gov.br). 
