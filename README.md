Salva & Guarda
==============

Este projeto é um protótipo agregando diversas ferramentas de scraping e
download direto de websites numa iniciativa do [Instituto
Socioambiental](https://www.socioambiental.org) para incorporar dados gerados
pelo Governo Brasileiro, garantindo a persistência dessas informações mesmo no
caso de serem retiradas do ar pelas suas instituições de origem.

Tais ferramentas e procedimentos foram criados em caráter emergencial para o
download da maior quantidade possível de informações diante da ameaça de apagão
de dados, como por exemplo nos casos envolvendo o Ministério do Meio Ambiente -
MMA e o Instituto de Pesquisas Espaciais - INPE.

Como se trata de um protótipo, o projeto ainda carece de um fluxo de trabalho
consolidado e de procedimentos mais automáticos para gestão de acervo, necessitando
ainda de uma série etapas manuais que não estão documentadas.

Arquitetura
-----------

Este projeto basicamente agrega diversas técnicas e softwares para download de dados
e armazena listas de sites e bases de dados relevantes para o download:

* [ckandumper](https://git.fluxo.info/ckandumper) é usado para baixar conteúdo listado em
  plataformas CKAN como o http://dados.gov.br.

* [grab-queue](https://git.fluxo.info/grab-queue) é usado para gerir fileiras de download
  de sites; o `grab-queue` baixa os sites através do [grab-site](https://github.com/ArchiveTeam/grab-site),
  que por sua vez usa um fork do [wpull](https://github.com/ArchiveTeam/wpull).

Instalação
----------

Recomendamos que você crie uma conta específica para a atividade de download
com suporte a sudo (privilégios administrativos):

    sudo adduser baixador
    sudo usermod -a -G sudo baixador
    sudo mkdir /var/lib/baixador
    sudo chown baixador. /var/lib/baixador

Entre com esta conta e clone o código do projeto:

    git clone --recursive https://gitlab.com/socioambiental/baixador /var/lib/code/baixador

Use o script provisionador numa máquina virtual ou servidor dedicado baseado em Debian
ou Ubuntu (testado usando Debian `sid` e Ubuntu `bionic`):

    cd /var/lib/baixador && bin/provision

Isso irá instalar diversas ferramentas para montar uma estação de download de emergência.
Algumas delas vão pedir a senha da conta para que possam instalar pacotes de sistema.

Ativando
--------

Após a instalação, é preciso ativar o ambiente virtual do python onde muitas aplicações
foram instaladas. Para isso, inclua os seguintes comandos no arquivo de incialização do
shell da conta que realizará a atividade de download:

    export PYENV_ROOT="$HOME/.pyenv"
    export PATH="$PYENV_ROOT/bin:$PATH"
    eval "$($HOME/.pyenv/bin/pyenv init -)"

    export PATH="/var/lib/baixador/bin:$PATH"

Ou seja, para que esses comandos sejam executados sempre que você logar com
esta conta, adicione as linhas anteriores no seu `~/.bashrrc`, `~/.profile` ou
similar.

Console de operações
--------------------

Este projeto conta com um console para controle e operação da estação de downloads.

    make console

Tal comando inicializará alguns serviços (servidor do grab-site e wayback machine) dentro
do multiplexador de terminal [GNU Screen](https://www.gnu.org/software/screen/).

Baixando sites
--------------

Caso você queria baixar os sites listados num dos lotes disponíveis nas pastas `batches` (endereços
já catalogados) ou `inventory` (endereços ainda a serem avaliados), certifique-se de ter algumas
centenas de GB disponíveis em disco e então rode um comando deste tipo:

    run-grab-queue-batch 05 /media/dados/sites/grab-site/batches --limit-concurrent=10

No caso, o batch `05` será iniciado e

* Armazenará os arquivos baixados em `/media/dados/sites/grab-site/batches`.
* Fará no máximo 10 downloads consecutivos para evitar exaustão de banda, processamento
  e memória.

### Compilando URLs

Diversas fontes podem ser utilizadas para a construção das listas de URLs a serem baixadas:

* Compilação manual.
* [Coletando o próprio histórico de um perfil de navegador](https://www.taricorp.net/2016/web-history-warc/).
* [CCrawlDNS](https://github.com/lgandx/CCrawlDNS).
* [theHarvester](https://github.com/laramies/theHarvester).
* [Domínios gov.br - Portal Brasileiro de Dados Abertos](http://dados.gov.br/dataset/dominios-gov-br).
* [Domínios gov.br Registrados - Gestão de Domínios](http://dominios.governoeletronico.gov.br).

### Monitorando downloads

O `grab-queue` basicamente dispara downloads usando o
[grab-site](ihttps://github.com/ArchiveTeam/grab-site), que por sua vez emite
notificações de status num dashboard disponível através do endereço
http://localhost:29000.

Através deste dashboard é possível acompanhar os downloads e configurar padrões de URLs a serem ignoradas
em cada download caso estes se encontrem em situações de loop ou obtendo páginas irrelevantes, como formulários de busca
ou calendários de navegação infinita.

### Monitorando a performance

Recomenda-se o uso de algum software para o monitoramento de uso de recursos do sistema, como por
exemplo o `netdata`:

    sudo apt install netdata

Após a instalação, o netada iniciará uma interface web acessível no endereço http://localhost:19999.

### Monitoramento remoto

Você pode acessar esses dashboards usando uma conexão SSH caso você esteja
usando para baixar dados uma máquina diferente da sua estação de trabalho. Basta criar uma entrada no seu
`~/.ssh/config` para o servidor remoto e os redirecionamentos:

    Host endereco-remoto-do-servidor
      User          baixador
      LocalForward 29000  127.0.0.1:29000  # grab-site
      LocalForward 6800   127.0.0.1:6800   # scrapyd
      LocalForward 8080   127.0.0.1:8080   # pywb
      LocalForward 19999  127.0.0.1:19999  # netdata

Com isso, os dashboards estarão disponvíeis no seu navegador a partir de endereços como http://localhost:29000
usando proxy SSH de forma transparente.

Replay de sites
---------------

Caso o [pywb](https://pywb.readthedocs.io/en/latest/) esteja configurado, você pode usar o seguinte
comando para inclusão de um dump na base de dados do Wayback Machine local:

    cd /media/dados/sites/wayback && \
    nice -n 19 ionice -c 3 -n 7 wb-manager add web PATH_TO/*.warc.gz

Plataformas de Dados Abertos
----------------------------

Também disponibilizamos um roteiro para o download da árvore e dados indexados nas plataformas do tipo CKAN
como http://dados.gov.br e http://dadosabertos.ibama.gov.br.

Exemplo de uso:

    bin/ckandumper --limit-concurrent=10 --wget="wget --no-check-certificate" \
                   --randomize http://dados.gov.br /media/dados/ckandumper/dados.gov.br

Compilando um inventário
------------------------

O inventário de dados baixados via grab-site/grab-queue pode ser produzido com o comando

    make inventory

Para atualizar automaticamente o inventário, configure o seguinte no crontab da conta de downloads:

    0 * * * * cd /home/baixador/code/baixador && /usr/bin/make inventory > /dev/null 2>&1

Interfaces web
--------------

### Backend

O console de operações iniciará um backend simples com detalhes dos arquivos baixados
em http://localhost:5000

### Frontend

Em concepção.

Referências
-----------

### Projetos

* [Brasil.io](https://brasil.io).
* [Radar Legislativo](https://radarlegislativo.org/).
* [Diário Livre](http://devcolab.each.usp.br/do/).
* [EIS Archiver](https://github.com/edgi-govdata-archiving/eis-WARC-archiver).

### Softwares

#### CKAN

* [ckan – The open source data portal software](https://ckan.org/).
* [ckanapi](https://github.com/ckan/ckanapi).
* [ckandumper](https://git.fluxo.info/ckandumper).

#### Archiving

* [wpull](https://github.com/ArchiveTeam/wpull).
* [grab-site](https://github.com/ArchiveTeam/grab-site).
* [wikiteam](https://github.com/WikiTeam/wikiteam).

Licença de Uso
--------------

Este projeto é duplamente licenciado em CC-BY-4.0 e GPLv3. As licenças estão disponíveis na pasta `licenses`.
