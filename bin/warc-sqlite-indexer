#!/usr/bin/env python3
#
# Index WARC file into a sqlite database.
#

# Requirements
import re
import argparse
from pprint import pprint
from warcio.archiveiterator import ArchiveIterator
from tqdm import tqdm
from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relation, sessionmaker

Base = declarative_base()

class WarcSqlite:
    """Index WARC file into a sqlite database"""

    def __init__(self, args):
        self.files      = args.file
        self.db         = args.db
        self.table_name = args.table
        self.debug      = args.debug
        self.progress   = args.progress
        self.engine     = create_engine('sqlite:///' + self.db)

        if args.match_uri != None:
            self.match_uri = args.match_uri
        else:
            self.match_uri = False

        class Metadata(Base):
            __tablename__ = 'meta'
            id            = Column(Integer, primary_key=True)
            file          = Column(String,  nullable=False, unique=False)
            table         = Column(String,  nullable=False, unique=False)

        # Dynamic table code thanks https://sparrigan.github.io/sql/sqla/2016/01/03/dynamic-tables.html
        # Fields names follow WARC spec but all strings lowercase and hifens replaced by underscores:
        self.table = { '__tablename__'       : self.table_name,
                       'id'                  : Column(Integer, primary_key=True),
                       'content_type'        : Column(String,  nullable=False, unique=False), # Content-Type
                       'content_length'      : Column(String,  nullable=False, unique=False), # Content-Length
                       'warc_type'           : Column(String,  nullable=False, unique=False), # WARC-Type
                       'warc_date'           : Column(String,  nullable=False, unique=False), # WARC-Date
                       'warc_record_id'      : Column(String,  nullable=False, unique=False), # WARC-Record-ID
                       'warc_target_uri'     : Column(String,  nullable=False, unique=False), # WARC-Target-URI
                       'warc_ip_address'     : Column(String,  nullable=False, unique=False), # WARC-IP-Address
                       'warc_block_digest'   : Column(String,  nullable=False, unique=False), # WARC-Block-Digest
                       'warc_payload_digest' : Column(String,  nullable=False, unique=False), # WARC-Payload-Digest
                       'warc_warcinfo_id'    : Column(String,  nullable=False, unique=False), # WARC-Warcinfo-ID
                       'statusline'          : Column(String,  nullable=False, unique=False),
                       'status'              : Column(Integer, nullable=False, unique=False),
                      }

        self.entry = type('Entry', (Base,), self.table)
        self.meta  = Metadata

        Base.metadata.create_all(self.engine)

    def hydrate(self):
        """Iterate over the WARC file and store records in a sqlite table"""

        Session = sessionmaker(bind=self.engine)
        session = Session()

        for file in self.files:
            with open(file, 'rb') as stream:
                # Save metadata
                try:
                    metadata = self.meta(file=file, table=self.table_name)

                    session.add(metadata)
                    session.commit()
                except:
                    session.rollback()

                if self.progress:
                    iterator = tqdm(ArchiveIterator(stream), "Processing " + file)
                else:
                    print("Processing " + file + '...')
                    iterator = ArchiveIterator(stream)

                for record in iterator:
                    if self.debug == True:
                        pprint(dir(record))
                        pprint(record.rec_type)
                        pprint(record.rec_headers)

                    if record.rec_type == 'response':
                        if self.debug == True:
                            pprint(record.rec_headers)

                        if self.match_uri != False:
                            uri = record.rec_headers.get_header('WARC-Target-URI'),

                            # Discard non-matching entries
                            if not re.match(self.match_uri, uri[0]):
                                if self.debug == True:
                                    print("Discarding " + uri[0] + "...")

                                continue

                        entry = self.entry(content_type        = record.rec_headers.get_header('Content-Type'),
                                           content_length      = record.rec_headers.get_header('Content-Length'),
                                           warc_type           = record.rec_headers.get_header('WARC-Type'),
                                           warc_date           = record.rec_headers.get_header('WARC-Date'),
                                           warc_record_id      = record.rec_headers.get_header('WARC-Record-ID'),
                                           warc_target_uri     = record.rec_headers.get_header('WARC-Target-URI'),
                                           warc_ip_address     = record.rec_headers.get_header('WARC-IP-Address'),
                                           warc_block_digest   = record.rec_headers.get_header('WARC-Block-Digest'),
                                           warc_payload_digest = record.rec_headers.get_header('WARC-Payload-Digest'),
                                           warc_warcinfo_id    = record.rec_headers.get_header('WARC-Warcinfo-ID'),
                                           statusline          = record.http_headers.statusline,
                                           status              = int(re.sub(r" .*$", "", record.http_headers.statusline)),
                                           )

                        try:
                            session.add(entry)
                            session.commit()
                        except:
                            session.rollback()

if __name__ == "__main__":
    # Parse CLI
    examples  = """ Example:

      warc-sqlite-indexer <file> <database> <table>
    """
    parser    = argparse.ArgumentParser(description='Index WARC file into a sqlite database.', epilog=examples, formatter_class=argparse.RawDescriptionHelpFormatter,)
    parser.add_argument('db',                                                         help='Destination database')
    parser.add_argument('table',                                                      help='Destination table')
    parser.add_argument('file',               nargs="+",                              help='WARC file')
    parser.add_argument('--match-uri',                                                help='Only index records whose URI matches a given regexp')
    parser.add_argument('--debug',            dest='debug',     action='store_true',  help='Enable debug')
    parser.add_argument('--no-debug',         dest='debug',     action='store_false', help='Disable debug')
    parser.add_argument('--progress',         dest='progress',  action='store_true',  help='Enable progress')
    parser.add_argument('--no-progress',      dest='progress',  action='store_false', help='Disable progress')
    parser.set_defaults(debug=False)
    parser.set_defaults(progress=True)
    args = parser.parse_args()

    # Initialize
    job = WarcSqlite(args)

    # Dispatch
    job.hydrate()
