#!/usr/bin/env bash
#
# Build website resource lists: extract fields from CDX files, apply URL regexp and sort for diffing.
#
# Each CDX file can have it's fields in an arbitrary order, so field extraction is differente for each CDX.
# As a WARC file can have content from multiple websites, we apply a regexp to limit results only to a given website.
# Then we sort the result.
#

# Parameters
BASENAME="`basename $0`"
DATA="/media/dados"
OLD_FILE="$1"
NEW_FILE="$2"
SITE_REGEXP_CLI="$3"
SITE_REGEXP_SQL="$3"

# Check
if [ -z "$SITE_REGEXP_SQL" ]; then
  echo "usage: $BASENAME <older-cdx> <newer-cdx> <filter-regexp-cli> <filter-regexp-sql>"
  exit 1
elif [ ! -e "$OLD_FILE" ] || [ ! -e "$NEW_FILE" ]; then
  echo "either <older-cdx> or <newer-cdx> does not exist"
  exit 1
fi

# Additional parameters
OLD_CDX="`basename $OLD_FILE`"
NEW_CDX="`basename $NEW_FILE`"
OLD_FOLDER="`dirname $OLD_FILE`"
NEW_FOLDER="`dirname $NEW_FILE`"

# Create filtered and sorted CDX files
head -1 $OLD_FILE                         > $OLD_CDX
grep "$SITE_REGEXP_CLI" $OLD_FILE | sort >> $OLD_CDX
head -1 $NEW_FILE                         > $NEW_CDX
grep "$SITE_REGEXP_CLI" $NEW_FILE | sort >> $NEW_CDX

#
# Older site dump
#

# CDX from the older site dump
#  CDX N b a m s k r M S V g
# Fields: 3 (a: url) and 6 (k: checksum)
cat $OLD_CDX | awk '{ print $3 " " $6 }' > $OLD_CDX.sums

# Make a simpler list from the older CDX
cat $OLD_CDX | awk '{ print $3 }' | sort -u > $OLD_CDX.list

# Make a status code list from the older CDX
cat $OLD_CDX | awk '{ print $3 " " $5 }' | sort -u > $OLD_CDX.status

# Logfile, if available
if [ -e "$OLD_FOLDER/wpull.log" ]; then
  grep "Fetched ‘$SITE_REGEXP_CLI" $OLD_FOLDER/wpull.log > $OLD_CDX.wpull.log
fi

#
# Newer site dump
#

# CDX from the newer site dump
#  CDX a b m s k S V g u
# Fields: 1 (a: url) and 5 (k: checksum)
cut -d ' ' -f 1,5 $NEW_CDX > $NEW_CDX.sums

# Make a simpler list from the newer CDX
cut -d ' ' -f 1 $NEW_CDX | sort -u > $NEW_CDX.list

# Make a status code list from the older CDX
cut -d ' ' -f 1,4 $NEW_CDX > $NEW_CDX.status

# Logfile, if available
if [ -e "$NEW_FOLDER/wpull.log" ]; then
  grep "Fetched ‘$SITE_REGEXP_CLI" $NEW_FOLDER/wpull.log > $NEW_CDX.wpull.log
fi

# Database comparison, if available
if [ -e "compare.db" ]; then
  sqlite3 compare.db "SELECT DISTINCT warc_target_uri FROM old WHERE warc_target_uri LIKE '%$SITE_REGEXP_SQL%' AND warc_target_uri NOT IN (SELECT DISTINCT warc_target_uri FROM new);" > missing.list
fi
