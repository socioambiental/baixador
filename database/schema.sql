--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.13
-- Dumped by pg_dump version 11.6 (Debian 11.6-0+deb10u1)

-- Started on 2020-02-10 16:22:17 UTC

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 6529 (class 1262 OID 5486837)
-- Name: db_socioambiental; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE db_socioambiental WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'pt_BR.UTF-8' LC_CTYPE = 'pt_BR.UTF-8';


ALTER DATABASE db_socioambiental OWNER TO postgres;

\connect db_socioambiental

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 6530 (class 0 OID 0)
-- Dependencies: 6529
-- Name: DATABASE db_socioambiental; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE db_socioambiental IS 'Banco de dados que guarda informações Socioambientais';


--
-- TOC entry 26 (class 2615 OID 8200902)
-- Name: db_baixador; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA db_baixador;


ALTER SCHEMA db_baixador OWNER TO postgres;

--
-- TOC entry 6531 (class 0 OID 0)
-- Dependencies: 26
-- Name: SCHEMA db_baixador; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA db_baixador IS 'Schema que armazena dados sobre o baixador ';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 1502 (class 1259 OID 8200918)
-- Name: batches; Type: TABLE; Schema: db_baixador; Owner: postgres
--

CREATE TABLE db_baixador.batches (
    id integer NOT NULL,
    data_inclusao timestamp without time zone,
    data_ultima_alteracao timestamp without time zone,
    descricao character varying(255)
);


ALTER TABLE db_baixador.batches OWNER TO postgres;

--
-- TOC entry 6532 (class 0 OID 0)
-- Dependencies: 1502
-- Name: TABLE batches; Type: COMMENT; Schema: db_baixador; Owner: postgres
--

COMMENT ON TABLE db_baixador.batches IS 'Tabela que armazena informações sobre os lotes de download dos sites';


--
-- TOC entry 6533 (class 0 OID 0)
-- Dependencies: 1502
-- Name: COLUMN batches.id; Type: COMMENT; Schema: db_baixador; Owner: postgres
--

COMMENT ON COLUMN db_baixador.batches.id IS 'ID único do lote dados a serem baixados';


--
-- TOC entry 6534 (class 0 OID 0)
-- Dependencies: 1502
-- Name: COLUMN batches.data_inclusao; Type: COMMENT; Schema: db_baixador; Owner: postgres
--

COMMENT ON COLUMN db_baixador.batches.data_inclusao IS 'Data da primeira vez que o lote foi baixado';


--
-- TOC entry 6535 (class 0 OID 0)
-- Dependencies: 1502
-- Name: COLUMN batches.data_ultima_alteracao; Type: COMMENT; Schema: db_baixador; Owner: postgres
--

COMMENT ON COLUMN db_baixador.batches.data_ultima_alteracao IS 'Data da ultima vez que o lote foi baixado';


--
-- TOC entry 6536 (class 0 OID 0)
-- Dependencies: 1502
-- Name: COLUMN batches.descricao; Type: COMMENT; Schema: db_baixador; Owner: postgres
--

COMMENT ON COLUMN db_baixador.batches.descricao IS 'Descritivo do que o lote se trata (que tipo de sites é o foco desse lote)';


--
-- TOC entry 1501 (class 1259 OID 8200916)
-- Name: batches_id_seq; Type: SEQUENCE; Schema: db_baixador; Owner: postgres
--

CREATE SEQUENCE db_baixador.batches_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE db_baixador.batches_id_seq OWNER TO postgres;

--
-- TOC entry 6538 (class 0 OID 0)
-- Dependencies: 1501
-- Name: batches_id_seq; Type: SEQUENCE OWNED BY; Schema: db_baixador; Owner: postgres
--

ALTER SEQUENCE db_baixador.batches_id_seq OWNED BY db_baixador.batches.id;


--
-- TOC entry 1506 (class 1259 OID 8201268)
-- Name: download; Type: TABLE; Schema: db_baixador; Owner: postgres
--

CREATE TABLE db_baixador.download (
    id integer NOT NULL,
    fonte_id bigint,
    data_inicio timestamp without time zone,
    data_fim timestamp without time zone,
    status_code integer,
    header text,
    caminho text
);


ALTER TABLE db_baixador.download OWNER TO postgres;

--
-- TOC entry 6539 (class 0 OID 0)
-- Dependencies: 1506
-- Name: TABLE download; Type: COMMENT; Schema: db_baixador; Owner: postgres
--

COMMENT ON TABLE db_baixador.download IS 'Tabela que armazena informações sobre o processo de download dos sites';


--
-- TOC entry 6540 (class 0 OID 0)
-- Dependencies: 1506
-- Name: COLUMN download.id; Type: COMMENT; Schema: db_baixador; Owner: postgres
--

COMMENT ON COLUMN db_baixador.download.id IS 'Tabela que armazena os processos de download';


--
-- TOC entry 6541 (class 0 OID 0)
-- Dependencies: 1506
-- Name: COLUMN download.fonte_id; Type: COMMENT; Schema: db_baixador; Owner: postgres
--

COMMENT ON COLUMN db_baixador.download.fonte_id IS 'ID único da fonte de informação';


--
-- TOC entry 6542 (class 0 OID 0)
-- Dependencies: 1506
-- Name: COLUMN download.data_inicio; Type: COMMENT; Schema: db_baixador; Owner: postgres
--

COMMENT ON COLUMN db_baixador.download.data_inicio IS 'Quando iniciou o processo de baixar a fonte';


--
-- TOC entry 6543 (class 0 OID 0)
-- Dependencies: 1506
-- Name: COLUMN download.data_fim; Type: COMMENT; Schema: db_baixador; Owner: postgres
--

COMMENT ON COLUMN db_baixador.download.data_fim IS 'Data que terminou o processo de download';


--
-- TOC entry 6544 (class 0 OID 0)
-- Dependencies: 1506
-- Name: COLUMN download.status_code; Type: COMMENT; Schema: db_baixador; Owner: postgres
--

COMMENT ON COLUMN db_baixador.download.status_code IS 'Status code (HTTP) da fonte no momento de download';


--
-- TOC entry 6545 (class 0 OID 0)
-- Dependencies: 1506
-- Name: COLUMN download.header; Type: COMMENT; Schema: db_baixador; Owner: postgres
--

COMMENT ON COLUMN db_baixador.download.header IS 'Gravação dos headers da conversa entre o baixador e o site';


--
-- TOC entry 6546 (class 0 OID 0)
-- Dependencies: 1506
-- Name: COLUMN download.caminho; Type: COMMENT; Schema: db_baixador; Owner: postgres
--

COMMENT ON COLUMN db_baixador.download.caminho IS 'Caminho (path) para download do site';


--
-- TOC entry 1505 (class 1259 OID 8201266)
-- Name: download_id_seq; Type: SEQUENCE; Schema: db_baixador; Owner: postgres
--

CREATE SEQUENCE db_baixador.download_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE db_baixador.download_id_seq OWNER TO postgres;

--
-- TOC entry 6548 (class 0 OID 0)
-- Dependencies: 1505
-- Name: download_id_seq; Type: SEQUENCE OWNED BY; Schema: db_baixador; Owner: postgres
--

ALTER SEQUENCE db_baixador.download_id_seq OWNED BY db_baixador.download.id;


--
-- TOC entry 1504 (class 1259 OID 8201256)
-- Name: fonte_informacao; Type: TABLE; Schema: db_baixador; Owner: postgres
--

CREATE TABLE db_baixador.fonte_informacao (
    id integer NOT NULL,
    nome character varying(255),
    descricao character varying(255),
    prioridade character varying(100),
    nivel_analise character varying(100),
    link text,
    data_inclusao timestamp without time zone,
    data_ultima_alteracao timestamp without time zone,
    batch_id integer,
    observacao text
);


ALTER TABLE db_baixador.fonte_informacao OWNER TO postgres;

--
-- TOC entry 6549 (class 0 OID 0)
-- Dependencies: 1504
-- Name: TABLE fonte_informacao; Type: COMMENT; Schema: db_baixador; Owner: postgres
--

COMMENT ON TABLE db_baixador.fonte_informacao IS 'Tabela que armazena links que são fontes do baixador de sites';


--
-- TOC entry 6550 (class 0 OID 0)
-- Dependencies: 1504
-- Name: COLUMN fonte_informacao.id; Type: COMMENT; Schema: db_baixador; Owner: postgres
--

COMMENT ON COLUMN db_baixador.fonte_informacao.id IS 'ID único da fonte de informação';


--
-- TOC entry 6551 (class 0 OID 0)
-- Dependencies: 1504
-- Name: COLUMN fonte_informacao.nome; Type: COMMENT; Schema: db_baixador; Owner: postgres
--

COMMENT ON COLUMN db_baixador.fonte_informacao.nome IS 'Nome do órgão detentor do site a ser baixado';


--
-- TOC entry 6552 (class 0 OID 0)
-- Dependencies: 1504
-- Name: COLUMN fonte_informacao.descricao; Type: COMMENT; Schema: db_baixador; Owner: postgres
--

COMMENT ON COLUMN db_baixador.fonte_informacao.descricao IS 'Descrição do site a ser baixado';


--
-- TOC entry 6553 (class 0 OID 0)
-- Dependencies: 1504
-- Name: COLUMN fonte_informacao.prioridade; Type: COMMENT; Schema: db_baixador; Owner: postgres
--

COMMENT ON COLUMN db_baixador.fonte_informacao.prioridade IS 'Define o nível de prioridade para download (alta, média ou baixa)';


--
-- TOC entry 6554 (class 0 OID 0)
-- Dependencies: 1504
-- Name: COLUMN fonte_informacao.nivel_analise; Type: COMMENT; Schema: db_baixador; Owner: postgres
--

COMMENT ON COLUMN db_baixador.fonte_informacao.nivel_analise IS 'Descreve o nível de análise sobre o conjunto de dado';


--
-- TOC entry 6555 (class 0 OID 0)
-- Dependencies: 1504
-- Name: COLUMN fonte_informacao.link; Type: COMMENT; Schema: db_baixador; Owner: postgres
--

COMMENT ON COLUMN db_baixador.fonte_informacao.link IS 'Link de origem para baixador de sites';


--
-- TOC entry 6556 (class 0 OID 0)
-- Dependencies: 1504
-- Name: COLUMN fonte_informacao.data_inclusao; Type: COMMENT; Schema: db_baixador; Owner: postgres
--

COMMENT ON COLUMN db_baixador.fonte_informacao.data_inclusao IS 'Data/Time da primeira ver q o site foi baixado';


--
-- TOC entry 6557 (class 0 OID 0)
-- Dependencies: 1504
-- Name: COLUMN fonte_informacao.data_ultima_alteracao; Type: COMMENT; Schema: db_baixador; Owner: postgres
--

COMMENT ON COLUMN db_baixador.fonte_informacao.data_ultima_alteracao IS 'Data da última vez que o site foi baixado';


--
-- TOC entry 6558 (class 0 OID 0)
-- Dependencies: 1504
-- Name: COLUMN fonte_informacao.batch_id; Type: COMMENT; Schema: db_baixador; Owner: postgres
--

COMMENT ON COLUMN db_baixador.fonte_informacao.batch_id IS 'ID único do lote para download';


--
-- TOC entry 6559 (class 0 OID 0)
-- Dependencies: 1504
-- Name: COLUMN fonte_informacao.observacao; Type: COMMENT; Schema: db_baixador; Owner: postgres
--

COMMENT ON COLUMN db_baixador.fonte_informacao.observacao IS 'Observações sobre a fonte de informação';


--
-- TOC entry 1503 (class 1259 OID 8201254)
-- Name: fonte_informacao_id_seq; Type: SEQUENCE; Schema: db_baixador; Owner: postgres
--

CREATE SEQUENCE db_baixador.fonte_informacao_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE db_baixador.fonte_informacao_id_seq OWNER TO postgres;

--
-- TOC entry 6561 (class 0 OID 0)
-- Dependencies: 1503
-- Name: fonte_informacao_id_seq; Type: SEQUENCE OWNED BY; Schema: db_baixador; Owner: postgres
--

ALTER SEQUENCE db_baixador.fonte_informacao_id_seq OWNED BY db_baixador.fonte_informacao.id;


--
-- TOC entry 6233 (class 2604 OID 8200921)
-- Name: batches id; Type: DEFAULT; Schema: db_baixador; Owner: postgres
--

ALTER TABLE ONLY db_baixador.batches ALTER COLUMN id SET DEFAULT nextval('db_baixador.batches_id_seq'::regclass);


--
-- TOC entry 6235 (class 2604 OID 8201271)
-- Name: download id; Type: DEFAULT; Schema: db_baixador; Owner: postgres
--

ALTER TABLE ONLY db_baixador.download ALTER COLUMN id SET DEFAULT nextval('db_baixador.download_id_seq'::regclass);


--
-- TOC entry 6234 (class 2604 OID 8201259)
-- Name: fonte_informacao id; Type: DEFAULT; Schema: db_baixador; Owner: postgres
--

ALTER TABLE ONLY db_baixador.fonte_informacao ALTER COLUMN id SET DEFAULT nextval('db_baixador.fonte_informacao_id_seq'::regclass);


--
-- TOC entry 6240 (class 2606 OID 8201682)
-- Name: fonte_informacao fonte_informacao_link_key; Type: CONSTRAINT; Schema: db_baixador; Owner: postgres
--

ALTER TABLE ONLY db_baixador.fonte_informacao
    ADD CONSTRAINT fonte_informacao_link_key UNIQUE (link);


--
-- TOC entry 6237 (class 2606 OID 8200923)
-- Name: batches pkbatches; Type: CONSTRAINT; Schema: db_baixador; Owner: postgres
--

ALTER TABLE ONLY db_baixador.batches
    ADD CONSTRAINT pkbatches PRIMARY KEY (id);


--
-- TOC entry 6245 (class 2606 OID 8201276)
-- Name: download pkdownload; Type: CONSTRAINT; Schema: db_baixador; Owner: postgres
--

ALTER TABLE ONLY db_baixador.download
    ADD CONSTRAINT pkdownload PRIMARY KEY (id);


--
-- TOC entry 6242 (class 2606 OID 8201264)
-- Name: fonte_informacao pkfonte; Type: CONSTRAINT; Schema: db_baixador; Owner: postgres
--

ALTER TABLE ONLY db_baixador.fonte_informacao
    ADD CONSTRAINT pkfonte PRIMARY KEY (id);


--
-- TOC entry 6238 (class 1259 OID 8201289)
-- Name: fki_batch_fonte; Type: INDEX; Schema: db_baixador; Owner: postgres
--

CREATE INDEX fki_batch_fonte ON db_baixador.fonte_informacao USING btree (batch_id);


--
-- TOC entry 6243 (class 1259 OID 8201283)
-- Name: fki_download_fonte_informacao; Type: INDEX; Schema: db_baixador; Owner: postgres
--

CREATE INDEX fki_download_fonte_informacao ON db_baixador.download USING btree (fonte_id);


--
-- TOC entry 6246 (class 2606 OID 8201284)
-- Name: fonte_informacao fk_batch_fonte; Type: FK CONSTRAINT; Schema: db_baixador; Owner: postgres
--

ALTER TABLE ONLY db_baixador.fonte_informacao
    ADD CONSTRAINT fk_batch_fonte FOREIGN KEY (batch_id) REFERENCES db_baixador.batches(id) ON DELETE RESTRICT;


--
-- TOC entry 6247 (class 2606 OID 8201278)
-- Name: download fk_download_fonte_informacao; Type: FK CONSTRAINT; Schema: db_baixador; Owner: postgres
--

ALTER TABLE ONLY db_baixador.download
    ADD CONSTRAINT fk_download_fonte_informacao FOREIGN KEY (fonte_id) REFERENCES db_baixador.fonte_informacao(id) ON DELETE CASCADE;


--
-- TOC entry 6537 (class 0 OID 0)
-- Dependencies: 1502
-- Name: TABLE batches; Type: ACL; Schema: db_baixador; Owner: postgres
--

GRANT SELECT ON TABLE db_baixador.batches TO baixador;


--
-- TOC entry 6547 (class 0 OID 0)
-- Dependencies: 1506
-- Name: TABLE download; Type: ACL; Schema: db_baixador; Owner: postgres
--

GRANT SELECT ON TABLE db_baixador.download TO baixador;


--
-- TOC entry 6560 (class 0 OID 0)
-- Dependencies: 1504
-- Name: TABLE fonte_informacao; Type: ACL; Schema: db_baixador; Owner: postgres
--

GRANT SELECT ON TABLE db_baixador.fonte_informacao TO baixador;


-- Completed on 2020-02-10 16:22:21 UTC

--
-- PostgreSQL database dump complete
--

