# TerraMA²

* Sobre: Análise do site TerraMA² (Plataforma de monitoramento, análise e alerta a extremos ambientais).
* URL: http://www.terrama2.dpi.inpe.br
* Status: fora do ar na data de análise.

## Análise

Existem cópias arquivadas deste site, por exemplo em
https://web.archive.org/web/20190508180913/http://www.terrama2.dpi.inpe.br/

Pelo menos parte do código usado pela plataforma encontra-se em
https://github.com/TerraMA2

Não há nenhum comunicado na lista de emails sobre a indisponibilidade do site:
http://listas.dpi.inpe.br/pipermail/terrama2-l/

Apenas com essas informações não é possível saber se o site está apenas
passando por manutenção ou foi deliberadamente retirado do ar, porém se essa
condição persistir por muito tempo haverá um forte indício de que existe alguma
situação política mantendo a plataforma desligada.
