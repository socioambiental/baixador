#!/bin/bash
#
# Build comparison queries for initial estimates.
#

#sqlite3 compare.db \
# "SELECT DISTINCT warc_target_uri FROM old WHERE warc_target_uri LIKE '%mma.gov.br%' AND warc_target_uri NOT IN (SELECT DISTINCT warc_target_uri FROM new);" \
# > missing_unsorted.list

sqlite3 compare.db \
 "SELECT DISTINCT warc_target_uri FROM old WHERE warc_target_uri LIKE 'http://mma.gov.br%' AND warc_target_uri NOT IN (SELECT DISTINCT warc_target_uri FROM new);" \
 | sort -u \
 > missing_http_no_www.list

sqlite3 compare.db \
 "SELECT DISTINCT warc_target_uri FROM old WHERE warc_target_uri LIKE 'http://www.mma.gov.br%' AND warc_target_uri NOT IN (SELECT DISTINCT warc_target_uri FROM new);" \
 | sort -u \
 > missing_http_www.list

sqlite3 compare.db \
 "SELECT DISTINCT warc_target_uri FROM old WHERE warc_target_uri LIKE 'https://mma.gov.br%' AND warc_target_uri NOT IN (SELECT DISTINCT warc_target_uri FROM new);" \
 | sort -u \
 > missing_https_no_www.list

sqlite3 compare.db \
 "SELECT DISTINCT warc_target_uri FROM old WHERE warc_target_uri LIKE 'https://www.mma.gov.br%' AND warc_target_uri NOT IN (SELECT DISTINCT warc_target_uri FROM new);" \
 | sort -u \
 > missing_https_www.list

#sqlite3 compare.db \
# "SELECT COUNT(DISTINCT warc_target_uri) FROM old WHERE warc_target_uri LIKE 'http://mma.gov.br%' AND warc_target_uri NOT IN (SELECT DISTINCT warc_target_uri FROM new);"

#sqlite3 compare.db \
# "SELECT COUNT(DISTINCT warc_target_uri) FROM old WHERE warc_target_uri LIKE 'http://www.mma.gov.br%' AND warc_target_uri NOT IN (SELECT DISTINCT warc_target_uri FROM new);"
