# MMA

* Sobre: Análise do site do Ministério do Meio Ambiente.
* URL: http://www.mma.gov.br

## Dados para comparação

* Versão de referência:
  * Data: 01/11/2018
  * URL: https://web.archive.org/web/20181101042034/http://www.mma.gov.br/
  * WARC: https://archive.fart.website/archivebot/viewer/job/em38i
  * CLI: `restore-from-archiveteam em38i /media/dados/www.mma.gov.br-2018-11-01`
* Versão para comparação:
  * Data: 09/09/2019
  * URL: https://wayback.socioambiental.org/web/20190709072141/http://www.mma.gov.br/
  * CLI: `grab-site --no-offsite-links --wpull-args="--monitor-disk 5000m --monitor-memory 500m" http://www.mma.gov.br`

## Roteiro

* [x] Baixar metatados de cada job: https://archive.fart.website/archivebot/viewer/job/em38i
* [x] Comparação dos arquivos CDX.
* [x] Logs dos downloads.
* [x] Indexar WARCs numa base de dados.
* [x] Normalizar URLs da base de dados: `https*:\/\/(www.)*mma.gov.br`

## Para análises futuras:

* [ ] Refazer download para checar diferenças.
